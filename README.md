# Type system challenge
## Intro
A friend of mine wants to replicate a Monopoly game.
There are cards, some of them are properties and have a color.
Some special ones have two sides, with different colors, and you can choose one of them.
## The problem
He was wondering how he could have a list of cards, with the type system enforcing that they all have the same color.
## Solution
You are of course encouraged to give it a try by yourself first.
Let's write some definitions.
### Colors
 ```ocaml
module Color: sig
  type red
  type black
  type _ t =
    | Red: red t
    | Black: black t
end = struct
  type red = unit
  type black = unit
  type _ t =
    | Red: red t
    | Black: black t
end
```
The little trick here is that we are using phantom types (the type parameter used by `Color.t`)
This allows us to embed information about what color is used at the type level.
### Cards
The cards we are interested in for our problem could be implemented this way.
```ocaml
type effect = Irrelevant

type 'a simple = {
  color: 'a Color.t;
  effect: effect;
}

type ('a, 'b) wild = 'a simple * 'b simple
```
the `effect` is `Irrelevant` to our problem here, but in the real implementation, that's where things would go.
Here, the type parameter (`'a`) will be filled in with the type corresponding to the color of the card, say, `Color.black`.
A wildcard is simply a 2-in-1.
### Keeping track of colors
```ocaml
type _ colored =
  | Simple: 'a simple -> 'a colored
  | First: ('a, _) wild -> 'a colored
  | Second: (_, 'a) wild -> 'a colored
 ```
The trick here is to tell in which part of the card we are interested (and retrieve the corresponding color)
### Some helpers
```ocaml
let simple color = { color; effect = Irrelevant }
let red, black = simple Red, simple Black
```
As we don't card about the effect of the cards, there won't be too much to put inside.
## "Et voilà!"
Here we have it. A type-safe list of cards, of a given color.
### Example:
```ocaml
let cards = [ Simple red; First (red, black); Second (black, red) ]
```
> `val cards: red colored list`
Here, if you try to be smart, and give a wrong color, or point to the wrong part of a wildcard, type system will reject your list.
