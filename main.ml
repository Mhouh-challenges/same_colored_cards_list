module Color: sig
  type red
  type black
  type _ t =
    | Red: red t
    | Black: black t
end = struct
  type red = unit
  type black = unit
  type _ t =
    | Red: red t
    | Black: black t
end

type effect = Irrelevant

type 'a simple = {
  color: 'a Color.t;
  effect: effect;
}

type ('a, 'b) wild = 'a simple * 'b simple

type _ colored =
  | Simple: 'a simple -> 'a colored
  | First: ('a, _) wild -> 'a colored
  | Second: (_, 'a) wild -> 'a colored

let simple color = { color; effect = Irrelevant }
let red, black = simple Red, simple Black

let cards = [ Simple red; First (red, black); Second (black, red) ]

let string_of_simple_card_color
  : type a. a simple -> string
  (* Necessary type anotation
   * for matching GADTs with different type parameters *)
  = fun { color; effect = _ } ->
    match color with
    | Red -> "red"
    | Black -> "black"

let string_of_colored card =
  begin
    match card with
    | Simple c -> string_of_simple_card_color c
    | First (f, s) ->
      string_of_simple_card_color f ^ " and " ^ (string_of_simple_card_color s)
    | Second (f, s) ->
      string_of_simple_card_color f ^ " and " ^ (string_of_simple_card_color s)
      (* The First and Second treatment, although identical, is repeated
       * because it's an OR pattern on GADT, using type equations.
       * see https://discuss.ocaml.org/t/gadt-and-or-pattern-matching/4154 *)
  end
  ^ " card"

let () =
  print_endline "List contains:";
  List.iter (fun c -> print_string " - "; c |> string_of_colored |> print_endline) cards
